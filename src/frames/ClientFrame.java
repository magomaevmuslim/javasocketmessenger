package frames;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Александр on 19.03.2016.
 */
public class ClientFrame extends JFrame implements Runnable{
    private static final int DEFAULT_WIDTH = 620;
    private static final int DEFAULT_HEIGHT = 380;
    private static final int DEFAULT_LOCATION_X = 300;
    private static final int DEFAULT_LOCATION_Y = 200;
    private String clientName;
    private String serverAddress = "25.57.70.180";
    private int serverPort = 6666;
    private DataInputStream in;
    private DataOutputStream out;
    private JTextArea dialogText;
    private JTextArea sendText;

    public ClientFrame(String name)
    {
        super("Клиент: " + name);
        clientName = name;
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocation(DEFAULT_LOCATION_X, DEFAULT_LOCATION_Y);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel content = new JPanel();
        content.setBackground(Color.BLACK);

        dialogText = new JTextArea(null, 13, 53);
        content.add(new JScrollPane(dialogText), BorderLayout.CENTER);

        sendText = new JTextArea(7, 43);
        content.add(new JScrollPane(sendText), BorderLayout.CENTER);

        JButton sendButton = new JButton("Отправить");
        content.add(sendButton);

        add(content);

        try {
            InetAddress ipAddress = InetAddress.getByName(serverAddress); // создаем объект который отображает вышеописанный IP-адрес.
            Socket socket = new Socket(ipAddress, serverPort); // создаем сокет используя IP-адрес и порт сервера.

            // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиентом.
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
            in = new DataInputStream(sin);
            out = new DataOutputStream(sout);
        } catch (Exception x) {
            x.printStackTrace();
        }

        sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String message = sendText.getText();
                sendText.setText("");
                try {
                    out.writeUTF(message); // отсылаем введенную строку текста серверу.
                    out.flush(); // заставляем поток закончить передачу данных.
                } catch (Exception x)
                {
                    x.printStackTrace();
                }




            }
        });
    }

    public void run() {
        String newLine;
        while (true) {
            try {
                newLine = in.readUTF(); // ждем пока сервер отошлет строку текста.
                dialogText.append(clientName + ": " + newLine + "\n");
            } catch (Exception x)
            {
                x.printStackTrace();
            }
        }
    }
}
