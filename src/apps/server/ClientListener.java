package apps.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Александр on 20.03.2016.
 */
public class ClientListener implements Runnable{
    private static ArrayList<DataOutputStream> outs = new ArrayList<>();
    private DataInputStream in;
    private String message;

    public ClientListener(Socket socket)
    {
        try {
            // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиенту.
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
            in = new DataInputStream(sin);
            outs.add(new DataOutputStream(sout));

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void run()
    {
        while(true) {
            try {
                message = in.readUTF(); // ожидаем пока клиент пришлет строку текста.
                for (DataOutputStream out : outs) {
                    out.writeUTF(message); // отсылаем клиенту обратно ту самую строку текста.
                    out.flush(); // заставляем поток закончить передачу данных.
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
}
