package apps.server;
import java.io.*;
import java.net.*;

/**
 * Created by Александр on 20.03.2016.
 */
public class ServerMain {
    private static int port = 6666;
    public static void main(String[] args)
    {
        while (true)
        {
            try {
                ServerSocket ss = new ServerSocket(port); // создаем сокет сервера и привязываем его к вышеуказанному порту
                Socket socket = ss.accept(); // заставляем сервер ждать подключений и выводим сообщение когда кто-то связался с сервером
                ClientListener cl = new ClientListener(socket);
                Thread myThready = new Thread(cl);	//Создание потока "myThready"
                myThready.start();

            } catch(Exception x) { x.printStackTrace(); }
        }
    }

}