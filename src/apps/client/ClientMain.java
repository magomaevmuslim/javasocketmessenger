package apps.client;

import frames.ClientFrame;

import java.awt.*;

/**
 * Created by Александр on 19.03.2016.
 */
public class ClientMain {
    private static String name = "";
    public static void main(String[] args)
    {
        if (args.length >= 1)
             name = args[0];
        else
            name = "Неизвестный";

        EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                ClientFrame cframe = new ClientFrame(name);
                Thread myThready = new Thread(cframe);	//Создание потока "myThready"
                myThready.start();
                cframe.setVisible(true);
            }
        });

    }
}
